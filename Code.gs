/*
### INSTRUCTIONS ### 
  1. Add the Campaigns that you want to exclude fromn the scripts in campaignNames between "". If you want to run the script for ALL your campaigns delete the campaign_1 and leave just the apostrophe inside the brackets. 
  2. Create a new spreadsheet file with 2 sheets inside, name one of them Report and the other one NegativeKeyword
  3. Select LANG between EN, IT, FR & DE
  4. In AUTOCHANGE select true if you want automatically apply the negative KWs or false if you just want to see the report. All Ad groups where negative KWs will applly, will also be labelled with 'EXA Really EXA'
  5. Once you copy the script, create another file and name it utils.gs and add the library, which you can find at the bottom of this document
*/

var campaignNames = ["campaign_1"];
var ssUrl = 'https://docs.google.com/spreadsheets/d/1-nm7eDdCfcXtSO8nYwIgA3sBy8YjNxs2P0k34Rf8r1s/edit#gid=1478817709';
var sheetReportName = 'Report';
var sheetNegativeName = 'NegativeKeyword';
var LANG = 'EN';
var AUTOCHANGE = false;

function main() {  
  if (LANG == 'IT'){
    var stopWords = stopWordsIT();
  }else if (LANG == 'EN'){
    var stopWords = stopWordsEN();
  } else if (LANG == 'FR'){
    var stopWords = stopWordsFR();
  } else {
    throw('LANG PROBLEM')
  }
  
  Logger.log(campaignNames.length)
  if (campaignNames.length > 0){
    Logger.log(campaignNames)
    var campaignsString = ""
    for (var i=0; i<campaignNames.length; i++){
      campaignsString = campaignsString + "'"  + campaignNames[i] + "', "
    }
    var report = AdWordsApp.report("SELECT Query, KeywordTextMatchingQuery, CampaignName, CampaignId, AdGroupId, KeywordId, QueryMatchTypeWithVariant, AdGroupName " +
                                   "FROM SEARCH_QUERY_PERFORMANCE_REPORT " + 
                                   "WHERE CampaignName NOT_IN " + "[" + campaignsString.slice(0, -2) + "] " +
                                   "DURING YESTERDAY");
  } else{
    var report = AdWordsApp.report("SELECT Query, KeywordTextMatchingQuery, CampaignName, CampaignId, AdGroupId, KeywordId, QueryMatchTypeWithVariant, AdGroupName " +
                                   "FROM SEARCH_QUERY_PERFORMANCE_REPORT " + 
                                   "DURING YESTERDAY");
  }
  
  
  var ss = SpreadsheetApp.openByUrl(ssUrl);
  var sheetReport = ss.getSheetByName(sheetReportName);
  var sheetNegative = ss.getSheetByName(sheetNegativeName);
  sheetNegative.appendRow(['Campaign Id', 'Ad Group Id', 'Keyword', 'Keyword Id', 'Criterion Type', 'Negative Keyword', 'Query', 'AdGroupName', 'CampaignName'])
  
  // Search LabelId
  var labels = AdWordsApp.labels().withCondition("Name = 'EXA_really_EXA'").get();
  if (labels.totalNumEntities() == 1){
    var label_exa = labels.next();
    Logger.log("Label '%s' is up", label_exa.getName());
  } else if (labels.totalNumEntities() == 0){
    Logger.log('Create Label');
    AdWordsApp.createLabel('EXA_really_EXA', "The Negative Keywords AdGroup Label for 'EXA_really_EXA'", '#4A148C')
    labels = AdWordsApp.labels().withCondition("Name = 'EXA_really_EXA'").get();
    var label = labels.next()
    Logger.log("Label '%s' created", label_exa.getName());
  }else{
    throw new Error("Too mutch labels 'EXA_really_EXA'!");
  }
  
  // Push report on SpreadSheet
  report.exportToSheet(sheetReport);
  var reportRows = report.rows();
  while(reportRows.hasNext()){
    var reportRow = reportRows.next();
    var matchType = reportRow['QueryMatchTypeWithVariant'];
    var adGroupId = reportRow['AdGroupId'];
    var campaignId = reportRow['CampaignId'];
    var campaignName = reportRow['CampaignName']
    var keywordId = reportRow['KeywordId'];
    var keyword = reportRow['KeywordTextMatchingQuery'];
    var query = reportRow['Query'].toLowerCase();
    var adGroupName = reportRow['AdGroupName']
    
    var keywordAdwIterator = AdWordsApp.keywords().withIds([[adGroupId, keywordId]]).get();
    
    if (keywordAdwIterator.hasNext()){
      var keywordAdw = keywordAdwIterator.next()
    }
    else {
      Logger.log(keywordAdwIterator)
    }
    if (keywordAdw != undefined){
      Logger.log('Problem with AdGroupId: %s - KeywordId: %s', adGroupId, keywordId)
    }
    if (keywordAdw != undefined && keywordAdw.getMatchType() == 'EXACT'){
      if (matchType == 'exact (close variant)'){
        var queryArray = query.split(/\s+/);

        for (var i = 0; i < queryArray.length; i++){
          if (stopWords.indexOf(queryArray[i]) >= 0){
            queryArray.splice(i, 1);
          }
          var wordQuery;
          if (LANG == 'IT'){
            wordQuery = "\\b" + queryArray[i].replace(/\w$/, '.') + "\\b";
          } else if (LANG == 'EN'){
            wordQuery = "\\b" + queryArray[i] + ".?\\b";
          } else if (LANG == 'FR'){
            wordQuery = "\\b" + queryArray[i] + ".?\\b";
          } else if (LANG == 'DE'){
            wordQuery = "\\b" + queryArray[i] + "(e|n|er)?\\b";;
          } else {
            throw('LANG PROBLEM')
          
          }
          
          var re = new RegExp(wordQuery, "i");
          if (re.test(keyword) == false){
            var adGroup = AdWordsApp.adGroups().withIds([adGroupId]).get().next();
            var adGroupNegativeKeywords = adGroup.negativeKeywords().withCondition('Text = "' + queryArray[i] +'"').get();
            if (adGroupNegativeKeywords.totalNumEntities() == 0){
              if (AUTOCHANGE == true){
                // Add Label
                if (adGroup.labels().withCondition("Name = 'EXA_really_EXA'").get().totalNumEntities() == 0){
                  Logger.log('Apply Label')
                  adGroup.applyLabel('EXA_really_EXA');
                }
              // Add Negative keuword in AdGroup
                Logger.log('Add Negative Keyword')
                adGroup.createNegativeKeyword('"' + queryArray[i] +'"');
              }
              Logger.log(queryArray[i]);
              Logger.log(keyword);
              sheetNegative.appendRow([campaignId, adGroupId, keyword,  keywordId, "Negative Phrase", queryArray[i], query, adGroupName, campaignName]);;
            }else {
              Logger.log("NegativeKeyword: '" + queryArray[i] + "' is in NegativeKeywordsAdGroup: " + adGroup.getName())
            }
          }
        }
      }
    }
    
  }
}