**Description**

---

## Step by step - Tutorial

* Go to **Tools** and then to **Scripts.**
    
* Add a new script.

* Paste the code and make sure to follow the instructions inside the script.

---

## Notes from Ruzi

1. If you authorize but get an error that **it�s not authorized, try to refresh the page and retry**. You might need to retry multiple times before it works.
2. If you have multiple campaigns that you need to exclude, **add commas in between them and add them in ��.** 
    > ie: "Personal_Exact_Mobile_Search","Competitors_Exact_Mobile_Search","Short Term Loan_Exact_Mobile_Search", etc etc

3. For instruction n.5 `Once you copy the script, create another file and name it utils.gs and add the library, which you can find at the bottom of this document`, you need to
    * Delete from the original script the library part (*screenshot 1*)
    * Create a separate file under the already created script on the account. How (*screenshot 2*)
        1. Click on the `+` sign on the left of the script
        2. Paste the library part inside
        3. Name it utils.gs