var IT = ["negli", "tue", "degl", "vostro", "stanno", "avrei", "avevano", "faceva", 
          "fece", "avessi", "stavo", "agli", "col", "faremo", "facemmo", "con", 
          "avute", "sei", "starei", "foste", "una", "starebbe", "dalla", "sugl", 
          "dello", "è", "dai", "ci", "non", "farà", "noi", "ebbe", "stessero", "nei", 
          "dov", "chi", "questi", "fummo", "che", "all", "tua", "sull", "tuoi", "dallo", 
          "dagli", "avrebbero", "eravamo", "stiate", "faceste", "nello", "mie", "quanti", 
          "stessi", "per", "facevamo", "avremo", "fai", "e", "ho", "facessi", "avranno", 
          "sulle", "i", "facciate", "sono", "miei", "sugli", "steste", "c", "alla", "avuti", 
          "degli", "nel", "voi", "farebbe", "avevi", "tuo", "dall", "vostre", "staremmo", 
          "dell", "farebbero", "avrai", "faccia", "abbiamo", "siamo", "avesti", "quanto", 
          "tutti", "coi", "starete", "facevo", "ebbi", "avrò", "sullo", "quelli", "faccio", 
          "stareste", "avevate", "nell", "siate", "di", "facevate", "avevo", "fosti", "l", 
          "starà", "questa", "saremo", "avevamo", "tra", "erano", "fossero", "facendo", 
          "stavano", "io", "le", "ne", "abbiate", "starebbero", "suoi", "avrebbe", "facevi", 
          "furono", "hanno", "alle", "da", "stai", "stessimo", "allo", "quanta", "sia", 
          "sareste", "stavamo", "aveste", "faremmo", "sui", "stesti", "sta", "a", "la", 
          "su", "nella", "avremmo", "hai", "avrà", "dal", "lo", "sua", "fui", "ai", "avessimo", 
          "nostri", "dagl", "come", "sarai", "avrete", "avreste", "avesse", "ed", "sarei", 
          "sul", "dei", "ma", "avendo", "della", "farò", "avuto", "lei", "sarete", "sto", 
          "avemmo", "farei", "saresti", "contro", "il", "era", "tutto", "farai", "facessero", 
          "cui", "ebbero", "mi", "saranno", "sarebbero", "dove", "facesti", "nostra", "sue", 
          "nostre", "negl", "tu", "mia", "eri", "del", "avete", "feci", "fossimo", "facesse", 
          "delle", "aveva", "fareste", "stiamo", "quante", "staresti", "suo", "anche", "nostro", 
          "in", "al", "lui", "si", "faresti", "eravate", "perché", "avresti", "quelle", "quella", 
          "sarà", "siete", "avuta", "siano", "sarò", "stia", "ha", "saremmo", "abbiano", "stiano", 
          "faranno", "fosse", "stetti", "staranno", "loro", "quale", "staremo", "più", "se", 
          "questo", "abbia", "fu", "stavi", "stavate", "mio", "fanno", "facevano", "facciamo", 
          "stette", "facessimo", "un", "li", "nelle", "vostra", "ad", "sulla", "avessero", 
          "stando", "sarebbe", "starai", "dalle", "facciano", "fossi", "vi", "agl", "o", 
          "essendo", "stava", "farete", "stemmo", "vostri", "quello", "uno", "stettero", 
          "starò", "ero", "fecero", "stesse", "ti", "gli", "queste"]

var EN = ["m", "re", "under", "hers", "while", "all", "again", "will", 
          "above", "t", "each", "below", "the", "haven", "shouldn", "don", "just", 
          "ve", "s", "at", "how", "on", "most", "having", "ain", "itself", "been", 
          "for", "by", "an", "it", "up", "herself", "few", "only", "be", "that",
          "this", "ma", "in", "no", "didn", "yourself", "is", "did", "his", "such", 
          "once", "theirs", "o", "which", "off", "aren", "himself", "we", "mustn", 
          "over", "should", "won", "against", "any", "between", "both", "myself", 
          "not", "further", "they", "she", "has", "when", "nor", "doing", "down", 
          "then", "d", "weren", "before", "wasn", "and", "what", "than", "other", 
          "your", "my", "who", "have", "were", "until", "here", "now", "doesn", 
          "out", "are", "had", "isn", "being", "their", "i", "these", "does", 
          "where", "why", "ourselves", "or", "into", "more", "hadn", "them", 
          "you", "shan", "wouldn", "there", "very", "its", "can", "y", "yours", 
          "ll", "to", "some", "was", "do", "her", "yourselves", "but", "needn", 
          "of", "a", "through", "from", "am", "me", "so", "with", "him", "whom", 
          "he", "themselves", "about", "if", "after", "those", "same", "our", 
          "because", "as", "couldn", "during", "own", "hasn", "too", "ours", "mightn"]

var FR = ["fut", "avait", "eus", "on", "tu", "tes", "ses", "votre", "étée", "aurais", 
          "étantes", "t", "furent", "eue", "j", "ou", "ayez", "n", "serais", "fussiez", 
          "était", "elle", "avons", "le", "eussiez", "mon", "toi", "es", "d", "aviez", 
          "pas", "auront", "aie", "je", "ne", "été", "avions", "eusses", "ayante", "aient", 
          "seriez", "étées", "qui", "fûtes", "sont", "serions", "eu", "ayantes", "ayant", 
          "par", "sois", "étés", "étiez", "seraient", "aura", "eussions", "en", "eux", 
          "serons", "ai", "vous", "aurez", "fût", "ayons", "ma", "son", "aurions", "avez", 
          "nous", "êtes", "sommes", "du", "ce", "eûmes", "serez", "eut", "aurai", "avaient", 
          "il", "eurent", "as", "que", "aurait", "même", "est", "auras", "étants", "de", 
          "soyons", "mais", "serait", "ces", "fusse", "auraient", "eûtes", "ta", "avais", 
          "sur", "étaient", "me", "mes", "se", "c", "qu", "moi", "eussent", "fussions", "leur", 
          "étante", "fussent", "fûmes", "ait", "et", "lui", "étions", "suis", "ton", "avec", 
          "serai", "au", "un", "soyez", "à", "nos", "s", "fusses", "dans", "sa", "aies", "des", 
          "y", "eût", "fus", "étant", "seras", "notre", "m", "étais", "soient", "seront", "pour", 
          "eusse", "la", "aurons", "sera", "auriez", "te", "aux", "ayants", "l", "soit", "ont", 
          "eues", "vos", "une"]

var DE = ["etwas", "als", "deinem", "manche", "keiner", "keinen", "jedem", "andern", "euren", "dich",
          "werden", "vor", "einiges", "sich", "ich", "welchem", "jenem", "während", "wirst", "dort", "will",
          "anderr", "soll", "dieser", "habe", "das", "dein", "euer", "desselben", "ein", "wie", "bist", "dann",
          "euch", "dieselben", "einig", "wo", "deines", "hin", "keinem", "ist", "andere", "anderm",
          "eurer", "einen", "über", "hinter", "an", "ohne", "nur", "meinem", "welcher", "allen", "nichts",
          "wenn", "sind", "anderes", "manchem", "mir", "jede", "doch", "einmal", "weil", "indem",
          "dazu", "ihres", "alles", "waren", "unseres", "werde", "einer", "solchem", "zur", "nun", "aber",
          "sondern", "ihren", "anders", "einigem", "mich", "dem", "dies", "warst", "seine", "des",
          "einem", "anderem", "anderen", "jeden", "jene", "jetzt", "machen", "also", "mit", "unser",
          "meiner", "was", "welche", "wieder", "ob", "für", "er", "ihnen", "jenen", "hab", "solchen",
          "man", "würden", "sollte", "seinem", "alle", "demselben", "im", "nicht", "eurem", "seiner",
          "einiger", "durch", "eines", "selbst", "jedes", "sonst", "bis", "solcher", "vom", "meinen", "zum",
          "denn", "ander", "gewesen", "keines", "der", "damit", "eures", "unsere", "noch", "ihn", "jener",
          "wird", "du", "ihm", "hier", "derer", "kein", "welches", "so", "würde", "anderer", "war",
          "dessen", "haben", "welchen", "können", "jenes", "aller", "von", "in", "solche", "unserem",
          "sie", "dasselbe", "musste", "sehr", "derselbe", "zwar", "hatte", "ihre", "jeder", "wollte",
          "könnte", "meines", "einige", "muss", "seinen", "solches", "wollen", "wir", "deiner", "deinen",
          "zwischen", "meine", "ihrer", "uns", "manches", "seines", "unter", "auch", "auf", "einigen",
          "kann", "manchen", "ins", "derselben", "oder", "zu", "diesen", "diesem", "denselben", "diese",
          "am", "den", "weg", "dir", "da", "daß", "eure", "keine", "gegen", "es", "weiter", "allem",
          "dieselbe", "um", "unseren", "eine", "ihrem", "bei", "deine", "bin", "mancher", "die", "nach",
          "aus", "und", "viel", "hat", "ihr", "hatten", "mein", "dieses", "sein"]


function stopWordsIT(){
  return IT;
}

function stopWordsEN(){
  return EN;
}

function stopwordsFR(){
  return FR;
}

function stopWordsDE(){
  return DE;
}
